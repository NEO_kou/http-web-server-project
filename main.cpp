#include "HttpServer.hpp"
#include<memory>
static void Usage(string str)
{
    cout << "Usage: " << str << " port " << endl;
}
void MyDaemon()
{
    //1. 忽略信号,SIGPIPE,SIGCHLD
    signal(SIGPIPE,SIG_IGN);
    signal(SIGCHLD,SIG_IGN);
    //2. 不要让自己成为组长
    if(fork()>0) exit(0);//子进程绝对不是组长
    //3. 调用setsid
    setsid();
    //4. 标准输入,标准输出,标准错误的重定向,守护进程不能直接向显示器打印信息(独立的一个进程)
    /* int devnull = open("dev/null",O_RDONLY | O_WRONLY);
    if(devnull > 0)
    {
        /* dup2(devnull,0);
        dup2(devnull,1);
        dup2(devnull,2); */
        /*dup2(0,devnull);
        dup2(1,devnull);
        dup2(2,devnull);
    }
    close(devnull); */
}

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        Usage(argv[0]);
        exit(4);
    }
    int port = atoi(argv[1]);
    MyDaemon();
    std::shared_ptr<HttpServer> http_server(new HttpServer(port));
    http_server->InitServer();
    http_server->Loop();
    return 0;
}