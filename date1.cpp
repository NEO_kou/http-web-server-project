#include"date.hpp"
#include<unistd.h>
bool GetQueryString(string& query)//获取到对应的参数
{
    string methon = getenv("METHOD");
    if(methon == "GET")
    {
       query = getenv("QUERY_STRING");
       return true;
    }

    else if(methon == "POST")
    {
        int cl = atoi(getenv("CONTENT_LENGTH"));
        char ch = 0;
        while(cl!=0)
        {
            read(0,&ch,1);
            query.push_back(ch);
            cl--;
        }
        return true;
    }
    else
    {
        return false;
    }
}
void CutString(string& in, string& oldyear, string& oldmonth, string& oldday, string& newyear, string& newmonth, string& newday, const string& sep)
{
    int pos1 = in.find(sep,0);
    if(pos1!=string::npos)
        oldyear = in.substr(0,pos1);
    int pos2 = in.find(sep,pos1+1);
    if(pos2!=string::npos)
        oldmonth = in.substr(pos1+sep.size(),pos2-pos1-1);
    int pos3 = in.find(sep,pos2+1);
    if(pos3!=string::npos)
        oldday = in.substr(pos2+sep.size(),pos3-pos2-1);
    int pos4 = in.find(sep,pos3+1);
    if(pos4!=string::npos)
        newyear = in.substr(pos3+sep.size(),pos4-pos3-1);
    int pos5 = in.find(sep,pos4+1);
    if(pos5!=string::npos)
        newmonth = in.substr(pos4+sep.size(),pos5-pos4-1);
    newday = in.substr(pos5+sep.size());
}

void CutString(std::string &in, const std::string &sep, std::string &out1, std::string &out2)
{
    auto pos = in.find(sep);
    if(std::string::npos != pos){
        out1 = in.substr(0, pos);
        out2 = in.substr(pos+sep.size());
    }
}
int main()
{
    string query_string;
    GetQueryString(query_string);
    string oldyear,oldmonth,oldday,newyear,newmonth,newday;
    CutString(query_string,oldyear,oldmonth,oldday,newyear,newmonth,newday,"&");
    string oy,om,od,ny,nm,nd,tmp;
    CutString(oldyear,"=",tmp,oy);
    CutString(oldmonth,"=",tmp,om);
    CutString(oldday,"=",tmp,od);
    CutString(newyear,"=",tmp,ny);
    CutString(newmonth,"=",tmp,nm);
    CutString(newday,"=",tmp,nd);
    Date old(atoi(oy.c_str()),atoi(om.c_str()),atoi(od.c_str()));
    Date now(atoi(ny.c_str()),atoi(nm.c_str()),atoi(nd.c_str()));
    if(old._legal == false || now._legal ==false)
        cout<<"<html><head><meta charset=\"utf-8\"></head><h1>输入的日期是非法的,请重新输入</h1><html>";
    else
    {
        /* int ret = now.operator-(old);
        cout<<"<html><head><meta charset=\"utf-8\"></head><h1>"<<"相差了: "<< ret <<" 天"<<"</h1><html>"; */
        int ret = now.operator-(old);
        string data("<html><head><meta charset=\"utf-8\"><title>寇文宇的网页日期计算器</title></head><style>body {text-align: center; border: 10px solid rgba(165, 42, 42, 0.596);padding: 50px;background-color: rgba(152, 199, 173, 0.205);}</style><body><h1>计算起始日期到结束日期相差了多少天</h1><form action=\"/date1\" method=\"GET\">起始日期:<input type=\"text\"name=\"oldyear\" value=\"0\">年<select  name=\"oldmonth\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option></select>月<select  name=\"oldday\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option><option value=\"13\">13</option><option value=\"14\">14</option><option value=\"15\">15</option><option value=\"16\">16</option><option value=\"17\">17</option><option value=\"18\">18</option><option value=\"19\">19</option><option value=\"20\">20</option><option value=\"21\">21</option><option value=\"22\">22</option><option value=\"23\">23</option><option value=\"24\">24</option><option value=\"25\">25</option><option value=\"26\">26</option><option value=\"27\">27</option><option value=\"28\">28</option><option value=\"29\">29</option><option value=\"30\">30</option><option value=\"31\">31</option></select>日<br><br>结束日期:<input type=\"text\"name=\"newyear\" value=\"0\">年<select  name=\"newmonth\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option></select>月<select  name=\"newday\"><option value=\"1\">1</option><option value=\"2\">2</option><option value=\"3\">3</option><option value=\"4\">4</option><option value=\"5\">5</option><option value=\"6\">6</option><option value=\"7\">7</option><option value=\"8\">8</option><option value=\"9\">9</option><option value=\"10\">10</option><option value=\"11\">11</option><option value=\"12\">12</option><option value=\"13\">13</option><option value=\"14\">14</option><option value=\"15\">15</option><option value=\"16\">16</option><option value=\"17\">17</option><option value=\"18\">18</option><option value=\"19\">19</option><option value=\"20\">20</option><option value=\"21\">21</option><option value=\"22\">22</option><option value=\"23\">23</option><option value=\"24\">24</option><option value=\"25\">25</option><option value=\"26\">26</option><option value=\"27\">27</option><option value=\"28\">28</option><option value=\"29\">29</option><option value=\"30\">30</option><option value=\"31\">31</option></select>日<br><br><!-- 提交按钮 --><input type=\"submit\" value=\"计算\"></form>");
        data += "<br><br>";
        data += "<h1><span style=\"color: red;\">相差了: ";
        data += to_string(ret);
        data += "天";
        data += "</span><h1>";
        cout<<data;
    }
    return 0;
    
}