#include<iostream>
#include<string>
#include<cstdlib>
#include<unistd.h>
using namespace std;

bool GetQueryString(string& query)//获取到对应的参数
{
    string methon = getenv("METHOD");
    if(methon == "GET")
    {
       query = getenv("QUERY_STRING");
       return true;
    }

    else if(methon == "POST")
    {
        int cl = atoi(getenv("CONTENT_LENGTH"));
        char ch = 0;
        while(cl!=0)
        {
            cout<<cl<<endl;
            read(0,&ch,1);
            query.push_back(ch);
            cl--;
        }
        return true;
    }
    else
    {
        return false;
    }
}

void CutString(string& in, string& left, string& right, string& op, const string& sep)
{
    int pos1 = in.find(sep);
    int pos2 = in.rfind(sep);
    if(pos1!=string::npos&&pos2!=string::npos)
    {
        left = in.substr(0,pos1);
        right = in.substr(pos1+sep.size(),pos2-pos1-1);
        op = in.substr(pos2+sep.size());
    }
}

int my_calculate(int x,int y,string op)
{
    if(op=="add")
        return x+y;
    if(op=="sub")
        return x-y;
    if(op=="mul")
        return x*y;
    if(op=="div")
        return x/y;
    if(op=="del")
        return x%y;
    if(op=="power")
    {
        int ret = 1;
        while(y>0)
        {
            ret*=x;
            y--;
        }
        return ret;
    }
}

int main()
{
    string query_string;
    GetQueryString(query_string);
    //数据格式: a=100&b=200(样例)
    string left;string right;string opall;
    CutString(query_string, left, right, opall, "&");
    string leftvalue, rightvalue, op;string tmp1,tmp2;
    CutString(left,tmp1,leftvalue,tmp2,"=");
    CutString(right,tmp1,rightvalue,tmp2,"=");
    CutString(opall,tmp1,op,tmp2,"=");   
    int x = atoi(leftvalue.c_str());
    int y = atoi(rightvalue.c_str());
    int ret;
    ret = my_calculate(x,y,op);
    cout<<"<html><head><meta charset=\"utf-8\"><head>";
    cout<<"<body>";
    cout<<"<h1>计算结果: </h1>";
    cout<<"<h2>"<<x<<" "<<op<<" "<<y<<" = "<< ret<<"</h2>";
    cout<<"</body>";
    cout<<"</html>";
    return 0;
}
