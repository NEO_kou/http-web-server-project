#include "date.hpp"
#include <unistd.h>
#include <time.h>
bool GetQueryString(string &query) // 获取到对应的参数
{
    string methon = getenv("METHOD");
    if (methon == "GET")
    {
        query = getenv("QUERY_STRING");
        return true;
    }

    else if (methon == "POST")
    {
        int cl = atoi(getenv("CONTENT_LENGTH"));
        char ch = 0;
        while (cl != 0)
        {
            cout << cl << endl;
            read(0, &ch, 1);
            query.push_back(ch);
            cl--;
        }
        return true;
    }
    else
    {
        return false;
    }
}

void CutString(string &in, string &oldyear, string &oldmonth, string &oldday, string &days, const string &sep)
{
    int pos1 = in.find(sep, 0);
    if (pos1 != string::npos)
        oldyear = in.substr(0, pos1);
    int pos2 = in.find(sep, pos1 + 1);
    if (pos2 != string::npos)
        oldmonth = in.substr(pos1 + sep.size(), pos2 - pos1 - 1);
    int pos3 = in.find(sep, pos2 + 1);
    if (pos3 != string::npos)
        oldday = in.substr(pos2 + sep.size(), pos3 - pos2 - 1);
    days = in.substr(pos3 + sep.size());
}

void CutString(std::string &in, const std::string &sep, std::string &out1, std::string &out2)
{
    auto pos = in.find(sep);
    if (std::string::npos != pos)
    {
        out1 = in.substr(0, pos);
        out2 = in.substr(pos + sep.size());
    }
}

int main()
{
    string query_string;
    GetQueryString(query_string);
    string oldyear, oldmonth, oldday, days;
    CutString(query_string, oldyear, oldmonth, oldday, days, "&");
    string oy, om, od, gap, tmp;
    CutString(oldyear, "=", tmp, oy);
    CutString(oldmonth, "=", tmp, om);
    CutString(oldday, "=", tmp, od);
    CutString(days, "=", tmp, gap);
    Date old(atoi(oy.c_str()), atoi(om.c_str()), atoi(od.c_str()));
    int x = atoi(gap.c_str());
    Date ret = old.operator+(x);
    if (old._legal == false)
        cout << "<html><head><meta charset=\"utf-8\"></head><h1>输入的日期是非法的,请重新输入</h1><html>";
    else
    {
        /* cout << "<html><head><meta charset=\"utf-8\"></head><body><h1>" << old << "再过" << x << " 天"
             << "是: " << ret << "</h1></body><html>"; */
        string data("<html>\
<head>\
<meta charset=\"utf-8\">\
<title>寇文宇的网页日期计算器</title>\
</head>\
<style>\
    body {\
        text-align: center;\
        border: 10px solid rgba(165, 42, 42, 0.596);\
        padding: 50px;\
        background-color: rgba(152, 199, 173, 0.205);\
    }\
</style>\
<body>\
<h1>计算从起始日期过了X天后的日期</h1>\
    <form action=\"/date2\" method=\"GET\">\
        起始日期:<input type=\"text\"name=\"oldyear\" value=\"0\">年\
<select  name=\"oldmonth\">\
    <option value=\"1\">1</option>\
    <option value=\"2\">2</option>\
    <option value=\"3\">3</option>\
    <option value=\"4\">4</option>\
    <option value=\"5\">5</option>\
    <option value=\"6\">6</option>\
    <option value=\"7\">7</option>\
    <option value=\"8\">8</option>\
    <option value=\"9\">9</option>\
    <option value=\"10\">10</option>\
    <option value=\"11\">11</option>\
    <option value=\"12\">12</option>\
</select>月\
<select  name=\"oldday\">\
    <option value=\"1\">1</option>\
    <option value=\"2\">2</option>\
    <option value=\"3\">3</option>\
    <option value=\"4\">4</option>\
    <option value=\"5\">5</option>\
    <option value=\"6\">6</option>\
    <option value=\"7\">7</option>\
    <option value=\"8\">8</option>\
    <option value=\"9\">9</option>\
    <option value=\"10\">10</option>\
    <option value=\"11\">11</option>\
    <option value=\"12\">12</option>\
    <option value=\"13\">13</option>\
    <option value=\"14\">14</option>\
    <option value=\"15\">15</option>\
    <option value=\"16\">16</option>\
    <option value=\"17\">17</option>\
    <option value=\"18\">18</option>\
    <option value=\"19\">19</option>\
    <option value=\"20\">20</option>\
    <option value=\"21\">21</option>\
    <option value=\"22\">22</option>\
    <option value=\"23\">23</option>\
    <option value=\"24\">24</option>\
    <option value=\"25\">25</option>\
    <option value=\"26\">26</option>\
    <option value=\"27\">27</option>\
    <option value=\"28\">28</option>\
    <option value=\"29\">29</option>\
    <option value=\"30\">30</option>\
    <option value=\"31\">31</option>\
</select>日\
        <br>\
        <p class=\"left-align\"><p>&nbsp;</p>想要跳过:<input type=\"text\"name=\"days\" value=\"0\">天</p>\
        <br><br>\
        <input type=\"submit\" value=\"计算\">\
    </form>");
data += "<br><br>";
data += "<h1>";
cout<<data<<"<span style=\"color: red;\">"<< old << "再过" << x << " 天" << "是: " << ret << "</span>" << "</h1></body><html>";
    }
        
    return 0;
    



}