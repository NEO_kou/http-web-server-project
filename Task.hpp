#pragma once
#include<iostream>
#include"Protocol.hpp"

using namespace std;
class Task{
public:
    Task()
    {}
    Task(int sock)
        :_sock(sock)
    {}
    ~Task(){}

    void ProcessOn()
    {
        _handler(_sock);
    }
private:
    int _sock;
    CallBack _handler;//设置回调
};
