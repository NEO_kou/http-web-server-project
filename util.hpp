#pragma once
#include <iostream>
#include <vector>
#include <string>
#include <sys/types.h>
#include <sys/socket.h>
using namespace std;

// util是一个工具类,里面有字符串的处理方法
class Util
{
public:
    /* static void CutString(const string &str, const string &sep, vector<string> *out)
    {
        size_t start = 0;
        while (start < str.size())
        {
            auto pos = str.find(sep, start);
            if (pos == string::npos)
                break;
            out->push_back(str.substr(start, pos - start));
            start += pos - start;
            start += sep.size();
        }
        if (start < str.size())
            out->push_back(str.substr(start));
    } */

    // 不同平台下的分割符可能不同,将所有的分隔符都变成\n, 1. \r\n 2. \n 3. \r
    static int ReadLine(int sock, string &out)
    {
        char ch = 'x';
        while (ch != '\n')
        {
            ssize_t s = recv(sock, &ch, 1, 0);
            if (s > 0)
            {
                if (ch == '\r') // 将/r/n转换为/n或者将/r变成/n
                {
                    // 把recv的flag设置为MSG_PEEK,此时缓冲区的数据只能被我们看,不会被取走,数据窥探功能
                    recv(sock, &ch, 1, MSG_PEEK);
                    if (ch == '\n')            // 一定是\r\n
                        recv(sock, &ch, 1, 0); // 将头部的\n拿走,\r就被忽略了,没有插入到out中
                    else ch = '\n';
                }
                // 1. 普通字符 2. \n
                out.push_back(ch);
            }
            else if (s == 0) // 对方关闭连接
            {
                return 0;
            }
            else
            {
                return -1;
            }
        }
        return out.size();
    }

    static bool CutString(const string& str, string& key_out,string& value_out,string sep)
    {
        size_t pos = str.find(sep);
        if(pos!=string::npos)
        {
            key_out = str.substr(0,pos);
            value_out = str.substr(pos+sep.size());
            return true;
        }
        return false;
    }
};