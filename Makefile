.PHONY:all
all:httpserver calculator date1 date2
httpserver:main.cpp
	g++ -o $@ $^ -std=c++11 -lpthread
calculator:test_cgi.cpp
	g++ -o $@ $^ -std=c++11
date1:date1.cpp
	g++ -o $@ $^ -std=c++11
date2:date2.cpp
	g++ -o $@ $^ -std=c++11

.PHONY:clean
clean:
	rm -f httpserver
	rm -rf issue
	rm -f calculator
	rm -f date1
	rm -f date2

.PHONY:issue
issue:
	mkdir -p issue
	cp httpserver issue
	cp -rf wwwroot issue
	mv calculator issue/wwwroot
	mv date1 issue/wwwroot
	mv date2 issue/wwwroot