#include "TcpServer.hpp"
#include"Protocol.hpp"
#include<signal.h>
#include"Task.hpp"
#include"ThreadPool.hpp"
class HttpServer
{
public:
    HttpServer(int port = PORT)
        : _port(port),_stop(false)
    {}
    void InitServer()
    {
        signal(SIGPIPE, SIG_IGN);//要忽略sigpipe信号,若不忽略,在写入时可能直接崩溃
        //_tcpServer = TcpServer::GetInstance(_port);
    }
    void Loop()
    {
        //int listensock = _tcpServer->Sock();
        while (!_stop)
        {
            TcpServer* tsvr = TcpServer::GetInstance(_port);
            struct sockaddr_in peer;
            socklen_t len = sizeof(peer);
            int sock = accept(tsvr->Sock(), (struct sockaddr *)&peer, &len);//获取客户端的ip和端口号
            if(sock<0)
            {
                continue;
            }
            logMessage(DEBUG,"accept success");
            Task task(sock);
            ThreadPool::GetInstance()->PushTask(task);
        }
    }
    ~HttpServer()
    {}
private:
    int _port;
    bool _stop;
};