#pragma once

#include <iostream>
#include <assert.h>
using namespace std;
class Date
{
	friend ostream &operator<<(ostream &out, const Date &d); // 使用友元函数解决类外函数不能访问类中私密成员的问题
	friend istream &operator>>(istream &in, Date &d);

public:
	Date *operator&() // 自己不写系统会自动生成一个
	{
		return this;
		// 如果不想让人得到地址: return nullptr;
	}
	// 构造函数会频繁调用,放在类中作为inline
	Date(int year = 1, int month = 1, int day = 1) // 构造函数
	{
		_year = year;
		_month = month;
		_day = day;
		if (_month > 12 && _day > GetMonthDay(_year, _month))
			_legal = false;
	}
	Date &operator=(const Date &d) // 传参和返回用引用
	{
		if (this != &d) // 保证不自己给自己赋值
		{
			_year = d._year;
			_month = d._month;
			_day = d._day;
		}
		return *this;
	}
	bool CheckDate() // 检查日期是否正确
	{
		if (_year >= 1 && _month > 0 && _month < 13 && _day > 0 && _day <= GetMonthDay(_year, _month))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	int GetMonthDay(int year, int month)
	{
		static int day[13] = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // 将数组放在静态区,调用次数多了也不会很消耗栈帧
		if (month == 2 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}

		else
		{
			return day[month];
		}
	}
	int GetMonthDay2(int year, int month)
	{
		static int day[13] = {0, 31, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30}; // 将数组放在静态区,调用次数多了也不会很消耗栈帧
		if (month == 3 && ((year % 4 == 0 && year % 100 != 0) || (year % 400 == 0)))
		{
			return 29;
		}

		else
		{
			return day[month];
		}
	}

bool operator==(const Date& d)const
{
	return d._year == _year
		&& d._month == _month
		&& d._day == _day;
}


bool operator!=(const Date& d)const
{
	return !(*this == d);
}

//d1>d3(d)
bool operator>(const Date& d)const
{
	if (_year > d._year)
	{
		return true;
	}
	else if (_year==d._year&&_month > d._month)
	{
		return true;
	}
	else if (_year==d._year&&_month==d._month&&_day > d._day)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool operator>=(const Date& d)const
{
	return (*this > d) || (*this == d);
}

bool operator<(const Date& d)const
{
	return !(*this >= d);
}

bool operator<=(const Date& d)const
{
	return !(*this > d);
}

Date& operator+=(int day)//这里实现的功能是+=,因为d1本身的值改变了.并且这里用传引用返回?
{
	if (day < 0)//当操作符为负数时复用减法
	{
		return *this -= -day;
	}

	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month == 13)
		{
			_month = 1;
			_year++;
		}
	}
	return *this;//this为指向当前对象的指针,*this就是日期类本身
}


Date operator+(int day)const //出了作用域ret就销毁了,所以不能用引用返回
{
	Date ret = *this;
	ret += day;
	return ret;
}

Date& operator++()//前置++
{
	*this += 1;
	return *this;
	//return *this +=1;
}

Date operator++(int)//后置++,只能用值传递返回
{
	Date tmp(*this);
	*this += 1;
	return tmp;
}

Date& operator-=(int day)
{
	if (day < 0)
	{
		return *this += -day;
	}

	_day -= day;
	while (_day <= 0)
	{
		--_month;
		if (_month == 0)
		{
			_month = 12;
			_year--;
		}
		_day += GetMonthDay(_year, _month);
	}

	return *this;
}


Date operator-(int day)const//运算符重载的复用
{
	Date ret = *this;
	ret -= day;
	return ret;
}

int operator-(const Date& d)//日期-日期=天数
{
	//进行了很多运算符的复用
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (*this < d)
	{
		max = d;
		min = *this;
		flag = -1;
	}
	int n = 0;
	while (min != max)
	{
		min++;
		n++;
	}
	return n * flag;
}

Date& operator--()//前置--
{
	*this -= 1;
	return *this;
}

Date operator--(int)//后置--
{
	Date tmp(*this);
	*this -= 1;
	return tmp;
}

public:
	bool _legal = true;
	int _year;
	int _month;
	int _day;
};

ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日";
	return out;
}

istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}